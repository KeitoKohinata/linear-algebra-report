# %%
# cにより実行された行列式計算のグラフ化

import numpy as np
from numpy.linalg import det, inv, eig, solve
import time
import csv
import matplotlib.pyplot as plt
import matplotlib
import os
import sys

matplotlib.rcParams['font.family'] = 'Noto Sans CJK JP'

def output_figure(output, x, y):
  c_fold = "c"
  ysc_list = ["linear", "log",    "log"]
  xsc_list = ["linear", "linear", "log"]
  for xsc, ysc in zip(xsc_list, ysc_list):
    # 実行時間 result_time を png で保存する場合
    fig = plt.figure(figsize=(8,5))
    ax = fig.add_subplot(111)
    ax.set_xlabel("行列のサイズ")
    ax.set_ylabel("実行時間[s]")
    ax.set_xscale(xsc)
    ax.set_yscale(ysc)
    ax.scatter(x, y)
    plt.savefig("%s/%s_x%s_y%s.png"%(c_fold, output, xsc, ysc), dpi=300)
  
# %%
with open(file="c/det_result.csv", mode="r", newline="", encoding="utf8") as f:
  reader = csv.reader(f)
  data = np.array([row for row in reader])
x = np.array(data[:,0], dtype=np.int32)
y = np.array(data[:,1], dtype=np.int32)
output_figure("det", x, y)
# %%
