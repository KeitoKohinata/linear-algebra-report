# linear algebra report

figsは以前からあるグラフです．

figs_err と figs_time が新しく追加されてグラフです．一部figsと重なっている内容もあります．

たくさんグラフがありますが使うのはほんの一部になると思います．

グラフについて詳しく以降で話していきます．

## 図の命名方法

[処理名]\_[x軸のスケール]\_[y軸のスケール].png

## 現在考えている図の使い方の場合分け

計算時間の考察は，基本的にすべて対数グラフで行ったほうがいいと思います（これまでの議論を踏まえて）．

| 処理名 | 誤差考察の時 | 時間考察の時 |
| :-: | :-: | :-: |
| 行列式 | x:linear, y:log<br>(誤差が指数関数的に増加しているため) | x:log, y:log | 
| 連立方程式 | x:log, y:log<br>(誤差が概ねnのべき乗で増加しているため) | x:log, y:log | 
| 固有値・固有ベクトル | x:linear, y:linear<br>(規則的な変化は見られないため) | x:log, y:log | 
