# %%
import numpy as np
from numpy.linalg import det, inv, eig, solve
import time
import csv
import matplotlib.pyplot as plt
import matplotlib
import os
import sys

matplotlib.rcParams['font.family'] = 'Noto Sans CJK JP'

# n次正方行列Aの行列式を求める．
# LU分解:O(n^3)，対角要素の積:O(n) すなわち O(n^3)
"value = det(A)"

# n次正方行列Aとbの連立1次方程式の解を求める
# LU分解:O(n^3)，LUで方程式を解く:O(n^2) すなわちO(n^3) 
"x = solve(A,b)"
  
# n次正方行列Aの固有値と固有ベクトルを求める
# どのアルゴリズムかよくわからないため，オーダーはなし
# w[i] は固有値，v[:,i]は固有ベクトル．v自体は対角化行列と見ることができる．
"w,v = eig(A)"

def output_txt(s, output):
  """
  例題の実行結果を出力する関数
  
  Args
    `s` : 出力する文字列
    `output` : 出力するファイル名
  """
  os.makedirs("txt", exist_ok=True)
  with open("txt/"+output+".txt", mode="w", encoding="utf-8", newline="") as f:
    f.write(s)




def output_figure(n_range, output, result, result_time):
  """
  csv や png でデータを保存する関数．

  Args:
    `n_range` : 基本的にaccuracy_???等の元関数からもらってくる
    `output` : 元関数の関数名
    `result` : 元関数の精度に関する結果
    `result_time` : 元関数の実行時間に関する結果
    `yscale` : y軸を対数とするか通常とするか
  """

  # csvでも保存したい場合
  # os.makedirs("csv", exist_ok=True)
  # out = np.matrix([
  #   np.arange(n_range[0], n_range[1]), 
  #   result
  # ]).T
  # with open(output+"/accuracy_of_determinant.csv", mode="w", encoding="utf-8", newline="") as f:
  #   writer = csv.writer(f)
  #   writer.writerows(out)

  err_fold = "figs_err"
  time_fold = "figs_time"
  os.makedirs(err_fold, exist_ok=True)
  os.makedirs(time_fold, exist_ok=True)
  ysc_list = ["linear", "log",    "log"]
  xsc_list = ["linear", "linear", "log"]
  for xsc, ysc in zip(xsc_list, ysc_list):
    # 精度 result を png で保存する場合
    fig = plt.figure(figsize=(8,5))
    ax = fig.add_subplot(111)
    ax.set_xlabel("行列のサイズ")
    ax.set_ylabel("誤差(絶対値)")
    ax.set_xscale(xsc)
    ax.set_yscale(ysc)
    ax.scatter(np.arange(n_range[0], n_range[1]), result, )

    plt.savefig("%s/%s_x%s_y%s.png"%(err_fold, output, xsc, ysc), dpi=300)
    # plt.show()

    # 実行時間 result_time を png で保存する場合
    fig = plt.figure(figsize=(8,5))
    ax = fig.add_subplot(111)
    ax.set_xlabel("行列のサイズ")
    ax.set_ylabel("実行時間[s]")
    ax.set_xscale(xsc)
    ax.set_yscale(ysc)
    ax.scatter(np.arange(n_range[0], n_range[1]), result_time, )

    plt.savefig("%s/%s_x%s_y%s.png"%(time_fold, output, xsc, ysc), dpi=300)
    # plt.show()
  



def det_examples():
  """
  教科書にある行列式の値を．P61問17
  """
  s = ""
  matrixes = []
  matrixes.append(np.matrix([
    [-1,  2,  3],
    [ 4,  0,  2],
    [-2,  3, -4],
  ], dtype=np.float64))
  matrixes.append(np.matrix([
    [ 2,  4, -3,  4],
    [-5,  2,  1,  5],
    [-3,  4,  2, -1],
    [ 4,  6, -7, -2]
  ], dtype=np.float64))
  matrixes.append(np.matrix([
    [ 1,  2,  9, 10],
    [ 4,  3,  8, 11],
    [ 5,  6,  7, 12],
    [16, 15, 14, 13]
  ], dtype=np.float64))

  s += "行列式:P61問17\n"
  for i, mat in enumerate(matrixes, 1):
    s += "(%d)"%(i) + str(det(mat)) + "\n"
  s + "\n"

  output_txt(s, sys._getframe().f_code.co_name)


def solve_examples():
  """
  教科書にある連立1次方程式を解く．P67問22．
  """
  s = ""
  As = []
  bs = []
  As.append(np.matrix([
    [ 2,  3,  1],
    [ 1,  2,  3],
    [ 3,  1,  2]
  ], dtype=np.float64))
  bs.append(np.array([9,6,8], dtype=np.float64))
  As.append(np.matrix([
    [ 3, -1,  2],
    [-1,  5, -3],
    [ 1, -1,  3]
  ], dtype=np.float64))
  bs.append(np.array([-7, 35, -19], dtype=np.float64))

  s += "連立1次方程式:p67問22\n"
  for i, (A, b) in enumerate(zip(As, bs), 1):
    s += "(%d) "%(i) + str(solve(A,b)) + "\n"
  s += "\n"
  output_txt(s, sys._getframe().f_code.co_name)


def eig_examples():
  """
  教科書にある固有値・固有ベクトルを解く．P135問4
  """
  s = ""
  matrixes = []
  matrixes.append(np.matrix([
    [ 7, 10],
    [-3, -4],
  ], dtype=np.float64))
  matrixes.append(np.matrix([
    [1,  2,  3],
    [0,  1, -3],
    [0, -3,  1],
  ], dtype=np.float64))
  matrixes.append(np.matrix([
    [2,  0,  0],
    [0, -1,  0],
    [0,  0,  3],
  ], dtype=np.float64))

  s += "固有値・固有ベクトル:P135問4\n"
  for i, mat in enumerate(matrixes, 1):
    w,v = eig(mat)
    s += "(%d)\n"%i
    for weight, vector in zip(w, v.T):
      s+= "固有値: " + str(weight) + " 固有ベクトル: " + str(vector) + "\n"
  s += "\n"
  output_txt(s, sys._getframe().f_code.co_name)



def accuracy_of_determinant(n_range=(100, 1001), scale=0.25, output=True):
  """
  行列式の計算精度を検証用関数

  Args:
    `n_range` : 精度を検証する行列の大きさの範囲
    `scale` : 一様分布の範囲．0 ~ scale が行列の各要素に代入される．ただし，一部の行の要素は A[0] に等しくなる．
    `output` : グラフをoutputするかどうか
  """
  # 1000次正方行列ともなると，誤差が大きく行列式の値が無限大となってしまうため，乱数の範囲を小さく
  result = []
  result_time = []
  for n in range(n_range[0], n_range[1]):
    # 0~scale の範囲の値を持つ行列を生成
    A = np.random.rand(n,n) * scale

    # 2つの行を定数倍とし，正解の行列式の値が0となるよう設定
    # すなわち行列式の値そのものが誤差
    A[int(n/2)] = A[0]

    s = time.time()
    det_A = det(A)
    e = time.time()

    result_time.append(e-s)
    result.append(det(A))

  result = np.array(result)
  if output:
    output_figure(n_range, "test", result, result_time)
  print("determinant finished.")

  return result, result_time


def accuracy_of_solve(n_range=(100, 1001), scale=0.5, output=True):
  """
  連立1次方程式の計算精度を検証用関数

  Args:
    `n_range` : 精度を検証する行列の大きさの範囲
    `scale` : 一様分布の範囲．0 ~ scale が行列の各要素に代入される．
    `output` : グラフをoutputするかどうか
  """
  # 1000次正方行列ともなると，誤差が大きく行列式の値が無限大となってしまうため，乱数の範囲を小さく(scale=0.5)
  result = []
  result_time = []
  for n in range(n_range[0], n_range[1]):
    # 0~scale の範囲の値を持つ行列を生成
    A = np.random.rand(n,n) * scale
    b = np.random.rand(n)

    # A と b の solve は， np.float64 で行わなければならないが，その後の精度検証では np.float64 で行う必要がなく，より高い精度で検証を行うために np.float128を使用
    s = time.time()
    x = solve(A,b)
    e = time.time()
    result_time.append(e-s)

    x = x.astype(np.float128)
    A = A.astype(np.float128)
    b = b.astype(np.float128)

    # 
    err_ave = np.sum(np.abs(A@x-b))/n
    # print(err_ave)
    result.append(err_ave)

  result = np.array(result)
  if output:
    output_figure(n_range, "solve", result, result_time)

  print("solve finished.")
  return result, result_time


def accuracy_of_eig(n_range=(100, 1001), output=True):
  """
  固有値，固有ベクトルの精度を検証．精度の検証に逆行列計算は用いない．

  Args:
    `n_range` : 精度を検証する行列の大きさの範囲
    `output` : グラフをoutputするかどうか
  """
  mother_A = np.zeros([n_range[1]-1, n_range[1]-1], dtype=np.float64)

  for i in range(n_range[1]-1):
    for j in range(i+1,n_range[1]-1):
      # この10は適当
      mother_A[i,j] = np.random.randn()*10 

  result = []
  result_time = []
  for n in range(n_range[0], n_range[1]):
    A = mother_A[:n, :n].copy()
    A = np.diag(np.linspace(1,11,n,dtype=np.float64,endpoint=False))  + mother_A[:n, :n]

    s = time.time()
    w,v = eig(A)
    e = time.time()

    result_time.append(e-s)

    # 検証を精度よくするたｍ以下略
    A = A.astype(np.float128)
    v = v.astype(np.float128)
    w = w.astype(np.float128)
    
    # vは対角化行列である
    # (vA) - (wv) の要素の和を求める．本来なら0
    err_ave = np.sum(np.abs(A@v-v@w))/n

    # print("err_ave", err_ave)
    result.append(err_ave)
    # print("eig", n)
  
  result = np.array(result)
  if output:
    output_figure(n_range, "eig", result, result_time)

  print("accuracy_of_eig finished.")
  return result, result_time


def main():
  # det_examples()
  # solve_examples()
  # eig_examples()
  accuracy_of_determinant()
  # accuracy_of_solve()
  # accuracy_of_eig()
  # print(result[-100:])

  # plt.plot(np.arange(len(result[-10:])), result[-10:])
  # plt.show()


if __name__=="__main__":
  main()

# %%
