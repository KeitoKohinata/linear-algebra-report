#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/time.h>
#define NMAX 64

int N;

void* Calloc(int byte, int n){
  void* m = calloc(byte, n);
  if(m == NULL){
    fprintf(stderr, "メモリエラー");
    exit(1);
  }
  return m;
}

void* Malloc(int n){
  void* m = malloc(n);
  if(m == NULL){
    fprintf(stderr, "メモリエラー");
    exit(1);
  }
  return m;
}


double** Calloc2(int n){
  double** mem = (double**)Malloc(sizeof(double*)*n);
  for(int i = 0; i < n; i++){
    mem[i] = (double*)Calloc(sizeof(double), n);
  }
  return mem;
}


void setRandomValue(double** A){
  srand((unsigned int)time(NULL));
  for(int i = 0; i < N; i++){
    for(int j = 0; j < N; j++){
      A[i][j] = (double)rand()/RAND_MAX;
    }
  }
}

void SCAN(double** A, char s[NMAX]){
  FILE *fp;
  
  if((fp = fopen(s,"r"))==NULL)
    printf("ファイルを開けませんen");
  else{
    for(int i=0; i<N; i++){
      double a[N];
      for(int j=0; j<N; j++){
        fscanf(fp,"%lf",&a[j]);
        A[i][j] = a[j];
      }
    }
    fclose(fp);
  }
}

void SHOW_NxN(double** A){
  int i, j;
  for(i=0; i<N; i++){
    for(j=0; j<N; j++){
      printf("%f ",A[i][j]);
    }
    printf("\n");
  }
}

void set_L(double** L){
  for(int i=0; i<N; i++)
    L[i][i] = 1;
}

void u_1k(double** U, double** A){
  for(int k=0; k<N; k++)
    U[0][k] = A[0][k];
}

void l_j1(double** L, double** A, double** U){
  for(int j=1; j<N; j++)
    L[j][0] = A[j][0]/U[0][0];
}

/**
 * @brief u_jk を計算する
 * 
 * @param U 上三角行列
 * @param L 下三角行列 (対角成分が1)
 * @param A 元の行列
 * @param j 行を表すインデックス
 */
void u_jk(double** U, double** L, double** A, int j){
  for(int k=j; k<N; k++){ 
    double t=0;
    for(int s=0; s<j; s++)
      t += L[j][s]*U[s][k];
    
    U[j][k] = A[j][k] - t;
  }
}

void l_jk(double** U, double** L, double** A, int k){
  for(int j=k+1; j<N; j++){
    double t=0;
    for(int s=0; s<=k-1; s++)
      t += L[j][s]*U[s][k];
    
    L[j][k] = (A[j][k] - t)/U[k][k];
  }
}


int main(int argc, char** argv) {
  // N = 10;
  // N = atoi(argv[1]);
  struct timeval start, end;
  long s, e;
  for(N = 100; N < 1000; N++){
    double** A = Calloc2(N);
    double** L = Calloc2(N);
    double** U = Calloc2(N);
    setRandomValue(A);

    // printf("使用するファイルを入力してください:");
    // scanf("%s",name);
    
    // SCAN(A,"test_lu.txt");
    

    gettimeofday(&start, NULL);
    set_L(L);
    u_1k(U, A);
    l_j1(L, A, U);
    
    for(int q=1; q<N-1; q++){
      u_jk(U, L, A, q);
      l_jk(U, L, A, q);
    }
    u_jk(U, L, A, N-1);
    gettimeofday(&end, NULL);

    s = start.tv_usec;
    e = end.tv_usec;
    // printf("行列の大きさは， %d です\n", N);
    // printf("実行時間は， %ld us です\n", e-s);
    if (e-s > 0){
      printf("%d, %ld\n", N, e-s);
    }
    
    // printf("A\n");
    // SHOW_NxN(A);
    // printf("L\n");
    // SHOW_NxN(L);
    // printf("U\n");
    // SHOW_NxN(U);
    free(A);
    free(U);
    free(L);

  }
  

  return 0;
}

// 10
// 3,000~4,000
// 10,000~40,000